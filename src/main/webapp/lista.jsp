<%-- 
    Document   : newjsplista
    Created on : 25-04-2021, 14:03:57
    Author     : gamers
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.mycompany.enviolibros.entity.Reservalibros"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Reservalibros> solicitudes = (List<Reservalibros>) request.getAttribute("listaSolicitudes");
    Iterator<Reservalibros> itsolicitudes = solicitudes.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="EnvioController" method="POST">
        <h1>Reserva tu libro!</h1>
        
        <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>Nombre </th>
                                 
                    <th> </th>
         
                    </thead>
                    <tbody>
                        <%while (itsolicitudes.hasNext()) {
                       Reservalibros cm = itsolicitudes.next();%>
                        <tr>
                            <td><%= cm.getRut()%></td>
                            <td><%= cm.getNombre()%></td>
                            
                         
                 <td> <input type="radio" name="seleccion" value="<%= cm.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar Solicitud</button>
                    <button type="submit" name="accion" value="consultar" class="btn btn-success">Consultar Solicitud</button>

        </form>
    </body>
</html>
