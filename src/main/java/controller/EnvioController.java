/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.enviolibros.dao.ReservalibrosJpaController;
import com.mycompany.enviolibros.dao.exceptions.NonexistentEntityException;
import com.mycompany.enviolibros.entity.Reservalibros;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gamers
 */
@WebServlet(name = "EnvioController", urlPatterns = {"/EnvioController"})
public class EnvioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EnvioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EnvioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost");
            String accion=request.getParameter("accion");{
            
      if (accion.equals("ingreso")){
        try {

        String rut=request.getParameter("rut");
        String nombre=request.getParameter("nombre");
       
        
        Reservalibros  reservalibros=new Reservalibros();
        reservalibros.setRut(rut);
        reservalibros.setNombre(nombre);
          
        ReservalibrosJpaController dao=new ReservalibrosJpaController();
        
        
            dao.create(reservalibros);
         
        
          
         } catch (Exception ex) {
            Logger.getLogger(EnvioController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
           
         if (accion.equals("verlista")){
           
            ReservalibrosJpaController dao=new ReservalibrosJpaController(); 
                List<Reservalibros> lista=dao.findReservalibrosEntities();
                  request.setAttribute("listaSolicitudes",lista);
                  request.getRequestDispatcher("lista.jsp").forward(request, response);
                  
                  }
         if (accion.equals("eliminar")){
             
            try {
             String rut=request.getParameter("seleccion");
             ReservalibrosJpaController dao=new ReservalibrosJpaController();
            
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(EnvioController.class.getName()).log(Level.SEVERE, null, ex);
            }
             
         }
         if (accion.equals("consultar")){
             String rut=request.getParameter("seleccion");
             ReservalibrosJpaController dao=new ReservalibrosJpaController();
             Reservalibros reservar = dao.findReservalibros(rut);
             request.setAttribute("listaSolicitudes", reservar);
             request.getRequestDispatcher("consulta.jsp").forward(request, response);
             
         }
         
         if (accion.equals("editar")){
          try {
              String rut=request.getParameter("rut");
              String nombre=request.getParameter("nombre");
              
              Reservalibros  reservalibros=new Reservalibros();
              reservalibros.setRut(rut);
              reservalibros.setNombre(nombre);
              
              ReservalibrosJpaController dao=new ReservalibrosJpaController();
              
              
              dao.edit(reservalibros);
              
              List<Reservalibros> lista=dao.findReservalibrosEntities();
              request.setAttribute("listaSolicitudes",lista);
              request.setAttribute("listaSolicitudes",lista);
              request.getRequestDispatcher("lista.jsp").forward(request, response);
          } catch (Exception ex) {
              Logger.getLogger(EnvioController.class.getName()).log(Level.SEVERE, null, ex);
          }
             
         }
          processRequest(request, response);
            }
    }   
            
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
