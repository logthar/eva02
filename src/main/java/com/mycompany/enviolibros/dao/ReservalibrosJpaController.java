/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.enviolibros.dao;

import com.mycompany.enviolibros.dao.exceptions.NonexistentEntityException;
import com.mycompany.enviolibros.dao.exceptions.PreexistingEntityException;
import com.mycompany.enviolibros.entity.Reservalibros;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author galegria
 */
public class ReservalibrosJpaController implements Serializable {

    public ReservalibrosJpaController() {
        
    }
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Reservalibros reservalibros) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(reservalibros);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findReservalibros(reservalibros.getRut()) != null) {
                throw new PreexistingEntityException("Reservalibros " + reservalibros + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Reservalibros reservalibros) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            reservalibros = em.merge(reservalibros);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = reservalibros.getRut();
                if (findReservalibros(id) == null) {
                    throw new NonexistentEntityException("The reservalibros with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reservalibros reservalibros;
            try {
                reservalibros = em.getReference(Reservalibros.class, id);
                reservalibros.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reservalibros with id " + id + " no longer exists.", enfe);
            }
            em.remove(reservalibros);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reservalibros> findReservalibrosEntities() {
        return findReservalibrosEntities(true, -1, -1);
    }

    public List<Reservalibros> findReservalibrosEntities(int maxResults, int firstResult) {
        return findReservalibrosEntities(false, maxResults, firstResult);
    }

    private List<Reservalibros> findReservalibrosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Reservalibros.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Reservalibros findReservalibros(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Reservalibros.class, id);
        } finally {
            em.close();
        }
    }

    public int getReservalibrosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Reservalibros> rt = cq.from(Reservalibros.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
