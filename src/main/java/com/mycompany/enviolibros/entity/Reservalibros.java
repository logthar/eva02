/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.enviolibros.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gamers
 */
@Entity
@Table(name = "reservalibros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservalibros.findAll", query = "SELECT r FROM Reservalibros r"),
    @NamedQuery(name = "Reservalibros.findByRut", query = "SELECT r FROM Reservalibros r WHERE r.rut = :rut"),
    @NamedQuery(name = "Reservalibros.findByNombre", query = "SELECT r FROM Reservalibros r WHERE r.nombre = :nombre")})
public class Reservalibros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;

    public Reservalibros() {
    }

    public Reservalibros(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservalibros)) {
            return false;
        }
        Reservalibros other = (Reservalibros) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.enviolibros.entity.Reservalibros[ rut=" + rut + " ]";
    }
    
}
